<?php

namespace App\Http\Controllers;

use App\Models\CharityFinance;
use Illuminate\Http\Request;

class CharityFinanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CharityFinance  $charityFinance
     * @return \Illuminate\Http\Response
     */
    public function show(CharityFinance $charityFinance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CharityFinance  $charityFinance
     * @return \Illuminate\Http\Response
     */
    public function edit(CharityFinance $charityFinance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CharityFinance  $charityFinance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CharityFinance $charityFinance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CharityFinance  $charityFinance
     * @return \Illuminate\Http\Response
     */
    public function destroy(CharityFinance $charityFinance)
    {
        //
    }
}
