<?php

namespace App\Http\Controllers;

use App\Models\CharityPhoto;
use Illuminate\Http\Request;

class CharityPhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CharityPhoto  $charityPhoto
     * @return \Illuminate\Http\Response
     */
    public function show(CharityPhoto $charityPhoto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CharityPhoto  $charityPhoto
     * @return \Illuminate\Http\Response
     */
    public function edit(CharityPhoto $charityPhoto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CharityPhoto  $charityPhoto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CharityPhoto $charityPhoto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CharityPhoto  $charityPhoto
     * @return \Illuminate\Http\Response
     */
    public function destroy(CharityPhoto $charityPhoto)
    {
        //
    }
}
