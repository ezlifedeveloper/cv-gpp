<?php

namespace App\Http\Controllers;

use App\Models\Charity;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CharityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Charity::orderBy('date', 'desc')->get();
        $user = User::orderBy('name', 'asc')->get();

        return view('admin.charity')
            ->with('data', $data)
            ->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Charity::create([
            'name' => $request->name,
            'detail' => $request->detail,
            'target' => $request->target,
            'date' => $request->date,
            'report' => $request->report,
            'created_by' => Auth::id()
        ]);

        return redirect()->route('admin.charity.index')->with('alert', 'Successfully create charity event');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Charity  $charity
     * @return \Illuminate\Http\Response
     */
    public function show(Charity $charity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Charity  $charity
     * @return \Illuminate\Http\Response
     */
    public function edit(Charity $charity)
    {
        $participant = User::orderBy('name', 'asc')->get();

        return view('admin.charity.edit')
            ->with('participant', $participant)
            ->with('data', $charity);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Charity  $charity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Charity $charity)
    {
        $data = Charity::where($charity->id)->update([
            'name' => $request->name,
            'detail' => $request->detail,
            'target' => $request->target,
            'date' => $request->date,
            'report' => $request->report,
        ]);

        return redirect()->route('admin.charity.index')->with('alert', 'Successfully update charity event');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Charity  $charity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Charity $charity)
    {
        //
    }
}
