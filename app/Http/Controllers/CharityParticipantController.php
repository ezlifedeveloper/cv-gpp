<?php

namespace App\Http\Controllers;

use App\Models\CharityParticipant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CharityParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = CharityParticipant::create([
            'charity_id' => $request->id,
            'user_id' => $request->user,
            'created_by' => Auth::id()
        ]);

        return redirect()->route('admin.charity.index')->with('alert', 'Successfully add charity participant');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CharityParticipant  $charityParticipant
     * @return \Illuminate\Http\Response
     */
    public function show(CharityParticipant $charityParticipant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CharityParticipant  $charityParticipant
     * @return \Illuminate\Http\Response
     */
    public function edit(CharityParticipant $charityParticipant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CharityParticipant  $charityParticipant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CharityParticipant $charityParticipant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CharityParticipant  $charityParticipant
     * @return \Illuminate\Http\Response
     */
    public function destroy(CharityParticipant $charityParticipant)
    {
        //
    }
}
