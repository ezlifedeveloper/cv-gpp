<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        if (!Auth::check()) return view('frontend.login');
        else if (Auth::user()->role == 'superuser') return redirect()->route('admin.dashboard');
        else return redirect()->route('admin.profile');
    }

    public function login(Request $request)
    {
        $username = $request->username;
        $pwd = $request->password;

        if (Auth::attempt(['username' => $username, 'password' => $pwd])) {
            if (Auth::user()->role == 'superuser') return redirect()->route('admin.dashboard');
            else return redirect()->route('admin.profile');
        } else {
            return redirect()->back()->with('alert', 'Terjadi kesalahan dalam login. Cek kembali Username dan Password Anda');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->back();
    }
}
