<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Charity extends Model
{
    use HasFactory, SoftDeletes;
    protected $dates = ['date', 'deleted_at'];
    protected $fillable = ['name', 'detail', 'target', 'date', 'report', 'created_by'];
    protected $with = ['participants', 'photos'];

    public function participants()
    {
        return $this->hasMany(CharityParticipant::class, 'charity_id');
    }

    public function photos()
    {
        return $this->hasMany(CharityPhoto::class, 'charity_id');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
