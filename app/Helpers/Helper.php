<?php

namespace App\Helpers;

use App\Models\Bible;
use App\Models\Kepengurusan;
use App\Models\Mapping;
use App\Models\UserLog;
use Illuminate\Support\Facades\Auth;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class Helper
{
    public static function isAllowed($role)
    {
        return Auth::user()->role == $role;
    }

    public static function getNameInitial($string)
    {
        $acronym = "";
        foreach (explode(' ', $string) as $word)
            $acronym .= mb_substr($word, 0, 1, 'utf-8');

        return strtoupper(substr($acronym, 0, 2));
    }
    // public static function recordUserLog($id, $status, $lat = null, $lng = null)
    // {
    //     UserLog::create([
    //         'user_id' => $id,
    //         'status' => $status,
    //         'lat' => $lat,
    //         'lng' => $lng
    //     ]);
    // }

    public static function uploadFile($file, $fileName, $folder)
    {
        $size = $file->getSize();
        $ext = '.' . $file->getClientOriginalExtension();

        $fixedFileName = preg_replace("/[^a-zA-Z0-9.]/", "", $fileName);

        $destinationPath = public_path() . $folder;
        $uploadedname = time() . $fixedFileName . $ext;

        $file->move($destinationPath, $uploadedname);
        $destinationPath = env('APP_URL') . $folder;

        return $destinationPath . $uploadedname;
    }

    public static function cleanStr($string)
    {
        // Replaces all spaces with hyphens.
        $string = str_replace(' ', '-', $string);

        // Removes special chars.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        // Replaces multiple hyphens with single one.
        $string = preg_replace('/-+/', '-', $string);

        return $string;
    }
}
