@props(['user'])

<div>
    <label id="listbox-label" class="block text-sm font-medium text-gray-700" @click="$refs.button.focus()">
        Select User
    </label>
    <div class="mt-1 relative">
        <select id="user" name="user"
            class="mt-1 block w-full bg-white border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">

            @foreach ($user as $u)
            <option value={{$u->id}}>{{$u->name}}</option>
            @endforeach
        </select>
    </div>
</div>
