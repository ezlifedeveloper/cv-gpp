@props(['title'])

<!DOCTYPE html>
<html lang="en" class="h-full bg-gray-50 snippet-html js-focus-visible" data-js-focus-visible>

<x-head :title="$title.' - User Page'"></x-head>

<body class="h-full bg-gray-800 font-sans leading-normal tracking-normal">
    <div class="min-h-full bg-gray-50">
        <x-admin.sidebar :title="$title"></x-admin.sidebar>

        <div class="md:pl-64 flex flex-col flex-1">
            <main class="flex-1">
                <div class="py-6">
                    <div class="px-4 sm:px-6 md:pr-8 mb-4">
                        <h1 class="text-2xl font-semibold text-gray-900">{{ $title }}</h1>
                    </div>
                    <div class="px-4 sm:px-6 md:pr-8">
                        {{ $slot }}
                    </div>
                </div>
            </main>
        </div>
    </div>
</body>

</html>
