@props(['participant'])

<div x-data="{ tooltip: false }" class="relative z-30 inline-flex">
    @if($participant->photo)
    <img x-on:mouseover="tooltip = true" x-on:mouseleave="tooltip = false"
        src="{{ $participant->photo ?? 'https://images.unsplash.com/photo-1491528323818-fdd1faba62cc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80' }}"
        alt="{{ $participant->name }}"
        class="relative z-1 inline-block h-10 w-10 rounded-full ring-2 ring-white cursor-pointer shadow">
    @else
    <span
        class="relative z-1 inline-flex items-center justify-center h-10 w-10 rounded-full bg-blue-500 ring-white cursor-pointer shadow"
        x-on:mouseover="
        tooltip=true" x-on:mouseleave="tooltip = false">
        <span class="text-xl font-medium text-white">{{
            Helper::getNameInitial($participant->name)
            }}</span>
    </span>
    @endif

    <div class="relative" x-cloak x-show.transition.origin.bottom="tooltip">
        <div
            class="absolute top-0 z-10 w-32 p-2 -mt-1 text-sm leading-tight text-white transform -translate-x-1/2 -translate-y-full bg-orange-500 rounded-lg shadow-lg">
            {{ $participant->name }}
        </div>
        <svg class="absolute z-10 w-6 h-6 text-orange-500 transform -translate-x-12 -translate-y-3 fill-current stroke-current"
            width="8" height="8">
            <rect x="12" y="-10" width="8" height="8" transform="rotate(45)" />
        </svg>
    </div>
</div>
