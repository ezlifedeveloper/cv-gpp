@props(['name', 'date', 'detail', 'isLast'])

@php
$initial = Helper::getNameInitial($name);
if($initial == 'HS') $color = 'black';
else if($initial == 'BB') $color = 'rose-500';
else $color = 'green-500';
@endphp

<li>
    <div class="relative pb-8">
        @if(!$isLast) <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        @endif
        <div class="relative flex items-start space-x-3">
            <div class="relative">
                <span class="inline-flex items-center justify-center h-10 w-10 rounded-full bg-{{$color}} ">
                    <span class="text-xl font-medium leading-none text-white">{{ $initial }}</span>
                </span>
            </div>
            <div class="min-w-0 flex-1">
                <div>
                    <div class="text-sm">
                        <a href="#" class="font-medium text-gray-900">{{ $name }}</a>
                        <button type="button"
                            class="inline-flex items-center px-1 py-1.5 border border-transparent text-xs font-medium rounded-md
                            text-indigo-700 bg-indigo-100 hover:bg-indigo-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Edit Data
                        </button>
                    </div>
                    <p class="mt-0.5 text-sm text-gray-500">
                        {{ $date->format('d M Y').' ('.$date->diffForHumans().')' }}
                    </p>
                </div>
                <div class="mt-2 text-sm text-gray-700">
                    <p>
                        {{ $detail }}
                    </p>
                </div>
                {{ $slot }}
            </div>
        </div>
    </div>
</li>


{{--
<li>
    <div class="relative pb-8">
        <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        <div class="relative flex items-start space-x-3">
            <div>
                <div class="relative px-1">
                    <div class="h-8 w-8 bg-gray-100 rounded-full ring-8 ring-white flex items-center justify-center">
                        <!-- Heroicon name: solid/user-circle -->
                        <svg class="h-5 w-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                            fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd"
                                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>
                </div>
            </div>
            <div class="min-w-0 flex-1 py-1.5">
                <div class="text-sm text-gray-500">
                    <a href="#" class="font-medium text-gray-900">Hilary Mahy</a>
                    assigned
                    <a href="#" class="font-medium text-gray-900">Kristin Watson</a>
                    <span class="whitespace-nowrap">2d ago</span>
                </div>
            </div>
        </div>
    </div>
</li>
<li>
    <div class="relative pb-8">
        <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        <div class="relative flex items-start space-x-3">
            <div>
                <div class="relative px-1">
                    <div class="h-8 w-8 bg-gray-100 rounded-full ring-8 ring-white flex items-center justify-center">
                        <!-- Heroicon name: solid/tag -->
                        <svg class="h-5 w-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                            fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd"
                                d="M17.707 9.293a1 1 0 010 1.414l-7 7a1 1 0 01-1.414 0l-7-7A.997.997 0 012 10V5a3 3 0 013-3h5c.256 0 .512.098.707.293l7 7zM5 6a1 1 0 100-2 1 1 0 000 2z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>
                </div>
            </div>
            <div class="min-w-0 flex-1 py-0">
                <div class="text-sm leading-8 text-gray-500">
                    <span class="mr-0.5">
                        <a href="#" class="font-medium text-gray-900">Hilary Mahy</a>
                        added tags
                    </span>
                    <span class="mr-0.5">
                        <a href="#"
                            class="relative inline-flex items-center rounded-full border border-gray-300 px-3 py-0.5 text-sm">
                            <span class="absolute flex-shrink-0 flex items-center justify-center">
                                <span class="h-1.5 w-1.5 rounded-full bg-rose-500" aria-hidden="true"></span>
                            </span>
                            <span class="ml-3.5 font-medium text-gray-900">Bug</span>
                        </a>
                        <a href="#"
                            class="relative inline-flex items-center rounded-full border border-gray-300 px-3 py-0.5 text-sm">
                            <span class="absolute flex-shrink-0 flex items-center justify-center">
                                <span class="h-1.5 w-1.5 rounded-full bg-indigo-500" aria-hidden="true"></span>
                            </span>
                            <span class="ml-3.5 font-medium text-gray-900">Accessibility</span>
                        </a>
                    </span>
                    <span class="whitespace-nowrap">6h ago</span>
                </div>
            </div>
        </div>
    </div>
</li>

<li>
    <div class="relative pb-8">
        <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        <div class="relative flex items-start space-x-3">
            <div class="relative">
                <span class="h-10 w-10 rounded-full bg-green-500 flex items-center justify-center ring-8 ring-white">
                    <!-- Heroicon name: solid/check -->
                    <svg class="h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                        fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd"
                            d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                            clip-rule="evenodd" />
                    </svg>
                </span>
            </div>
            <div class="min-w-0 flex-1">
                <div>
                    <div class="text-sm">
                        <a href="#" class="font-medium text-gray-900">Eduardo Benz</a>
                    </div>
                    <p class="mt-0.5 text-sm text-gray-500">
                        Commented 6d ago
                    </p>
                </div>
                <div class="mt-2 text-sm text-gray-700">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tincidunt nunc ipsum
                        tempor purus vitae id. Morbi in vestibulum nec varius. Et diam cursus quis sed
                        purus nam.
                    </p>
                </div>
            </div>
        </div>
    </div>
</li>

<li>
    <div class="relative pb-8">
        <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        <div class="relative flex items-start space-x-3">
            <div>
                <div class="relative px-1">
                    <div class="h-8 w-8 bg-gray-100 rounded-full ring-8 ring-white flex items-center justify-center">
                        <!-- Heroicon name: solid/user-circle -->
                        <svg class="h-5 w-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                            fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd"
                                d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>
                </div>
            </div>
            <div class="min-w-0 flex-1 py-1.5">
                <div class="text-sm text-gray-500">
                    <a href="#" class="font-medium text-gray-900">Hilary Mahy</a>
                    assigned
                    <a href="#" class="font-medium text-gray-900">Kristin Watson</a>
                    <span class="whitespace-nowrap">2d ago</span>
                </div>
            </div>
        </div>
    </div>
</li>

<li>
    <div class="relative pb-8">
        <span class="absolute top-5 left-5 -ml-px h-full w-0.5 bg-gray-200" aria-hidden="true"></span>
        <div class="relative flex items-start space-x-3">
            <div>
                <div class="relative px-1">
                    <div class="h-8 w-8 bg-gray-100 rounded-full ring-8 ring-white flex items-center justify-center">
                        <!-- Heroicon name: solid/tag -->
                        <svg class="h-5 w-5 text-gray-500" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                            fill="currentColor" aria-hidden="true">
                            <path fill-rule="evenodd"
                                d="M17.707 9.293a1 1 0 010 1.414l-7 7a1 1 0 01-1.414 0l-7-7A.997.997 0 012 10V5a3 3 0 013-3h5c.256 0 .512.098.707.293l7 7zM5 6a1 1 0 100-2 1 1 0 000 2z"
                                clip-rule="evenodd" />
                        </svg>
                    </div>
                </div>
            </div>
            <div class="min-w-0 flex-1 py-0">
                <div class="text-sm leading-8 text-gray-500">
                    <span class="mr-0.5">
                        <a href="#" class="font-medium text-gray-900">Hilary Mahy</a>
                        added tags
                    </span>
                    <span class="mr-0.5">
                        <a href="#"
                            class="relative inline-flex items-center rounded-full border border-gray-300 px-3 py-0.5 text-sm">
                            <span class="absolute flex-shrink-0 flex items-center justify-center">
                                <span class="h-1.5 w-1.5 rounded-full bg-rose-500" aria-hidden="true"></span>
                            </span>
                            <span class="ml-3.5 font-medium text-gray-900">Bug</span>
                        </a>
                        <a href="#"
                            class="relative inline-flex items-center rounded-full border border-gray-300 px-3 py-0.5 text-sm">
                            <span class="absolute flex-shrink-0 flex items-center justify-center">
                                <span class="h-1.5 w-1.5 rounded-full bg-indigo-500" aria-hidden="true"></span>
                            </span>
                            <span class="ml-3.5 font-medium text-gray-900">Accessibility</span>
                        </a>
                    </span>
                    <span class="whitespace-nowrap">6h ago</span>
                </div>
            </div>
        </div>
    </div>
</li> --}}
