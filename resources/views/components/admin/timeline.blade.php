@props(['data'])

<ul role="list" class="-mb-8">
    @foreach($data as $d)
    <x-admin.timeline-item :name="$d->name" :date="$d->date" :detail="$d->detail" :isLast="$loop->last">
        <div class="text-sm mt-6">
            <x-admin.timeline-participant :id="$d->id" :participants="$d->participants">
            </x-admin.timeline-participant>
        </div>

        <div class="text-sm mt-8">
            <a href="#" class="font-medium text-gray-900">Documentation</a>

            <x-admin.timeline-photo :data="$data"></x-admin.timeline-photo>
        </div>
    </x-admin.timeline-item>
    @endforeach
</ul>
