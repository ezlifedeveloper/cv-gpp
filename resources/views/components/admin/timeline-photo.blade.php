@props(['data'])

<ul role="list" class="mt-2 grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-3 sm:gap-x-6 lg:grid-cols-4 xl:gap-x-8">
    @foreach ($data as $d)
    <x-admin.timeline-photo-item :photo="$d->photo"></x-admin.timeline-photo-item>
    @endforeach

    @if(Helper::isAllowed('superuser'))
    <button type="button"
        class="relative block w-full border-2 border-gray-300 border-dashed rounded-lg p-12 text-center hover:border-gray-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
        <span class="mt-2 block text-sm font-medium text-gray-900">
            Add More Photos
        </span>
    </button>
    @endif
</ul>
