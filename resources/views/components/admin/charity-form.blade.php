<div class="mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6">
    <div class="sm:col-span-6">
        <label for="name" class="block text-sm font-medium text-gray-700">
            Charity Title
        </label>
        <div class="mt-1">
            <input type="text" name="name" id="name"
                class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                placeholder="Membagikan sembako kepada siapa saja">
        </div>
    </div>

    <div class="sm:col-span-6">
        <label for="detail" class="block text-sm font-medium text-gray-700">
            Charity Detail
        </label>
        <div class="mt-1">
            <textarea rows="4" name="detail" id="detail"
                class="py-2 px-3 shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                placeholder="Kegiatan ini akan membagikan ... kepada .... orang. Makanan dibeli di ... "></textarea>
        </div>
    </div>

    <div class="sm:col-span-6">
        <label for="target" class="block text-sm font-medium text-gray-700">
            Charity Target
        </label>
        <div class="mt-1">
            <input type="text" name="target" id="target"
                class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                placeholder="Pemulung, Tukang Becak, Pasukan Kuning, dll">
        </div>
    </div>

    <div class="sm:col-span-6">
        <label for="date" class="block text-sm font-medium text-gray-700">
            Date Planned
        </label>
        <div class="mt-1">
            <input type="date" name="date" id="date"
                class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md">
        </div>
    </div>

    <div class="sm:col-span-6">
        <label for="report" class="block text-sm font-medium text-gray-700">
            Charity Report
        </label>
        <div class="mt-1">
            <textarea rows="4" name="report" id="report"
                class="py-2 px-3 shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                placeholder="Kegiatan telah dilaksanakan dengan ... kepada ... di ... "></textarea>
        </div>
    </div>
</div>
<div class="pt-5">
    <div class="flex justify-end">
        <button type="button"
            class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
            Cancel
        </button>
        <button type="submit"
            class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
            Save
        </button>
    </div>
</div>
