@props(['title'])

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title ?? 'Grasia Prima Perfekta'}}</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://cdn.tailwindcss.com/"></script>
    <script src="//unpkg.com/alpinejs" defer></script>
</head>
