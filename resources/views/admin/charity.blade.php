<x-admin.layout :title="'Charity'">
    <div class="mt-5 gap-5 lg:flex xs:grid xs:grid-cols-1 xs:gap-5">
        <div class="lg:w-2/3 xs:span-col-1 h-full bg-white mb-4 p-4 shadow rounded-lg">
            <div class="mb-4">
                <h3 class="text-lg font-medium text-gray-900">
                    Timeline
                </h3>
                <p class="mt-1 text-sm text-gray-500">
                    All charity event shown in timeline below
                </p>
            </div>
            <x-admin.timeline :data="$data"></x-admin.timeline>
        </div>

        @if(Helper::isAllowed('superuser'))
        <div class="lg:w-1/3 xs:span-col-1 h-full bg-white p-4 shadow rounded-lg">
            <div class="mb-4">
                <h3 class="text-lg font-medium text-gray-900">
                    Create Charity
                </h3>
                <p class="mt-1 text-sm text-gray-500">
                    Fill the form below to create new charity event
                </p>
            </div>

            <form method="POST" action="{{route('admin.charity.store')}}">
                @csrf
                <x-admin.charity-form></x-admin.charity-form>
            </form>
        </div>
        @endif
    </div>

    @if(Helper::isAllowed('superuser'))
    <x-admin.modal-add-participant :user="$user"></x-admin.modal-add-participant>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.openParticipantModal').on('click', function(e){
                $('#participant-charity-id').val($(this).data("value"));
                $('#addParticipantModal').removeClass('invisible');
            });
            $('.closeParticipantModal').on('click', function(e){
                $('#addParticipantModal').addClass('invisible');
            });
        });
    </script>
    @endif

</x-admin.layout>
