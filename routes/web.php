<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CharityController;
use App\Http\Controllers\CharityParticipantController;
use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.welcome');
});

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('login', [AuthController::class, 'login'])->name('login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');

Route::middleware('auth')->prefix('office')->name('admin.')->group(function () {

    Route::middleware('loggedin:superuser')->group(function () {

        Route::get('dashboard', function () {
            return view('admin.dashboard');
        })->name('dashboard');

        Route::resource('project', ProjectController::class);
    });

    Route::get('profile', function () {
        return view('admin.profile');
    })->name('profile');

    Route::resource('charity', CharityController::class);
    Route::resource('charity-participant', CharityParticipantController::class);

    Route::get('finance', function () {
        return view('admin.finance');
    })->name('finance');
});
